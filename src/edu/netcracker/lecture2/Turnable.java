package edu.netcracker.lecture2;

/**
 * Поворачиваемый объект
 */
public interface Turnable {
    /**
     * Поворачивает в заданное направление
     *
     * @param direction Направление
     */
    void turnTo(Direction direction);
}