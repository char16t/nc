package edu.netcracker.lecture2;

/**
 * Перемещаемый объект
 */
public interface Moveable {
    /**
     * Перемещает в точку (x, y)
     *
     * @param x Координата X
     * @param y Координата Y
     */
    void move(int x, int y);
}
