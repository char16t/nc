package edu.netcracker.lecture2;

public interface RoverCommand {
    void execute();
    void append(RoverCommand subcommands);
    RoverCommand getRoverCommand(int index);
}

