package edu.netcracker.lecture2;

import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {
        Rover r = new Rover();
        r.getVisor().setGround(new Ground(10, 10));
        r.move(19, 9);

        RoverCommand a =
                new ImportCommand(
                    new MoveCommand(r, 5, 6),
                    new TurnCommand(r, Direction.SOUTH)
                );
        RoverCommand b =
                new ImportCommand(
                    new TurnCommand(r, Direction.EAST),
                    new TurnCommand(r, Direction.WEST),
                    new ImportCommand(a)
                );
        r.getProgramParser().addRoverCommands(b);
        System.out.println(r.getDirection().toString());
    }
}
