package edu.netcracker.lecture2;

public class MoveCommand implements RoverCommand {
    private Moveable moveable;
    private int x;
    private int y;

    public MoveCommand(Moveable moveable) {
        this.moveable = moveable;
        this.x = 0;
        this.y = 0;
    }

    public MoveCommand(Moveable moveable, int x, int y) {
        this.moveable = moveable;
        this.x = x;
        this.y = y;
    }

    @Override
    public void execute() {
        moveable.move(x, y);
    }

    @Override
    public void append(RoverCommand subcommands) {
        throw new UnsupportedOperationException();
    }

    @Override
    public RoverCommand getRoverCommand(int index) {
        throw new UnsupportedOperationException();
    }

    public String toString() {
        return "move " + new Integer(x).toString() + " " + new Integer(y).toString();
    }
}
