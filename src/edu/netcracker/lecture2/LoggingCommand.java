package edu.netcracker.lecture2;

public class LoggingCommand implements RoverCommand {
    private RoverCommand command;

    public LoggingCommand(RoverCommand command){
        this.command = command;
    }

    @Override
    public void execute() {
        command.execute();
        System.out.println(command.toString());
    }

    @Override
    public void append(RoverCommand subcommands) {
        throw new UnsupportedOperationException();
    }

    @Override
    public RoverCommand getRoverCommand(int index) {
        throw new UnsupportedOperationException();
    }
}