package edu.netcracker.lecture2;

public class GroundVisorException extends RuntimeException {
    public GroundVisorException() {
    }

    public GroundVisorException(String message) {
        super(message);
    }

    public GroundVisorException(String message, Throwable cause) {
        super(message, cause);
    }

    public GroundVisorException(Throwable cause) {
        super(cause);
    }

    public GroundVisorException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
