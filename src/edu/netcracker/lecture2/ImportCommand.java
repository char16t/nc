package edu.netcracker.lecture2;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class ImportCommand implements RoverCommand {

    private List<RoverCommand> cmds;

    public ImportCommand(RoverCommand ... cmds) {
        this.cmds = new LinkedList<RoverCommand>();
        Collections.addAll(this.cmds, cmds);
    }

    @Override
    public void append(RoverCommand subcommands) {
        cmds.add(subcommands);
    }

    @Override
    public RoverCommand getRoverCommand(int index) {
        return cmds.get(index);
    }

    @Override
    public void execute() {
        for (RoverCommand subcommands : cmds) {
            subcommands.execute();
        }
    }

    public String toString() {
        String result = "-import start-\n";
        for (RoverCommand subcommands : cmds) {
            result += subcommands.toString() + "\n";
        }
        result += "-import end-";

        return result;
    }
}
