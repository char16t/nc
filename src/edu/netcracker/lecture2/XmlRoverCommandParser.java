package edu.netcracker.lecture2;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.util.LinkedList;
import java.util.List;

public class XmlRoverCommandParser {
    private Rover rover;
    private String file;
    private int cursor;
    private List<RoverCommand> roverCommands = new LinkedList<RoverCommand>();

    public XmlRoverCommandParser(Rover rover) throws IOException {
        this.rover = rover;
        this.cursor = 0;
        this.file = null;
    }

    public void setFile(String file) {
        this.file = file;
        try {
            initRoverCommands();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public RoverCommand readNextCommand() {
        if (cursor < roverCommands.size()) {
            return roverCommands.get(this.cursor++);
        }
        return null;
    }

    public List<RoverCommand> getRoverCommands() {
        return roverCommands;
    }

    public void addRoverCommands(RoverCommand ... cmds) throws IOException {
        for (RoverCommand command : cmds) {
            this.roverCommands.add(command);
            if (command instanceof ImportCommand) command.execute();
        }

    }

    public void addRoverCommands(List<RoverCommand> newRoverCommands) {
        for (RoverCommand command : newRoverCommands) {
            this.roverCommands.add(command);
        }
    }

    private void initRoverCommands() throws IOException {
        if (this.file == null) {
            return;
        }

        File fXmlFile = new File(this.file);
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = null;
        try {
            dBuilder = dbFactory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        try {
            Document doc = dBuilder.parse(fXmlFile);
            NodeList nList = doc.getElementsByTagName("commands");
            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node nNode = nList.item(temp);
                Element eElement = (Element) nNode;
                if (nNode.getNodeName().compareTo("move") == 0) {
                    int x = Integer.parseInt(eElement.getAttribute("x"));
                    int y = Integer.parseInt(eElement.getAttribute("y"));
                    this.roverCommands.add(new MoveCommand(this.rover, x, y));
                }
                else if(nNode.getNodeName().compareTo("turn") == 0) {
                    Direction direction = Direction.valueOf(eElement.getAttribute("to"));
                    this.roverCommands.add(new TurnCommand(this.rover, direction));
                }
            }
        } catch (SAXException e) {
            e.printStackTrace();
        }
    }
}