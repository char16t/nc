package edu.netcracker.lecture2;

/**
 * Направление
 *
 * NORTH  Север
 * EAST   Восток
 * SOUTH  Юг
 * WEST   Запад
 */
public enum Direction {
    NORTH, EAST, SOUTH, WEST
}