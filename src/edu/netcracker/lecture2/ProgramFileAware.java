package edu.netcracker.lecture2;

public interface ProgramFileAware {
    void executeProgramFile(String file);
}