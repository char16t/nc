package edu.netcracker.lecture2;

import java.io.IOException;

/**
 * Rover
 *
 * Имеет позицию в пространстве и направление движения
 */
public class Rover implements Turnable, Moveable, ProgramFileAware {
    private int x;
    private int y;
    private Direction direction;
    private GroundVisor visor = new GroundVisor();
    private XmlRoverCommandParser programParser;

    /**
     * Констркутор по-умолчанию. Создаёт Rover в точке (0, 0),
     * который движется на север
     */
    public Rover() throws IOException {
        this.x = 0;
        this.y = 0;
        this.direction = Direction.NORTH;
        this.programParser = new XmlRoverCommandParser(this);
    }

    /**
     * Констркутор. Создаёт Rover в точке (x, y),
     * который движется на север
     *
     * @param x Координата X
     * @param y Координата Y
     */
    public Rover(int x, int y) throws IOException {
        this.x = x;
        this.y = y;
        this.direction = Direction.NORTH;
        this.programParser = new XmlRoverCommandParser(this);
    }

    /**
     * Констркутор. Создаёт Rover в точке (x, y),
     * который движется в заданном направлении
     *
     * @param x          Координата X
     * @param y          Координата Y
     * @param direction  Направление движения
     */
    public Rover(int x, int y, Direction direction) throws IOException {
        this.x = x;
        this.y = y;
        this.direction = direction;
        this.programParser = new XmlRoverCommandParser(this);
    }

    public Rover(XmlRoverCommandParser programParser) {
        this.programParser = programParser;
    }

    /**
     * Устанавливает координату X
     *
     * @param x Коодината x
     */
    public void setX(int x) {
        this.x = x;
    }

    /**
     * Получает координату X
     *
     * @return Координата X
     */
    public int getX() {
        return this.x;
    }

    /**
     * Устанавливает координату Y
     *
     * @param y Координата Y
     */
    public void setY(int y) {
        this.y = y;
    }

    /**
     * Получает координату Y
     *
     * @return Координата Y
     */
    public int getY() {
        return this.y;
    }

    /**
     * Устанавливает направление двиижения
     *
     * @param direction Направление движения
     */
    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    /**
     * Получает направление движения
     *
     * @return Направление движения
     */
    public Direction getDirection() {
        return this.direction;
    }

    /**
     * Перемещает Rover в точку (x, y)
     *
     * @param x Координата X
     * @param y Координата Y
     */
    @Override
    public void move(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public void executeProgramFile(String file) {
        this.programParser.setFile(file);

        RoverCommand command = this.programParser.readNextCommand();
        while (command != null) {
            command.execute();
        }
    }

    /**
     * Поворачивает Rover в заданное направление
     *
     * @param direction Направление
     */
    @Override
    public void turnTo(Direction direction) {
        this.direction = direction;
    }

    public GroundVisor getVisor() {
        return visor;
    }

    public void setVisor(GroundVisor visor) {
        this.visor = visor;
    }

    public XmlRoverCommandParser getProgramParser() throws IOException {
        return programParser;
    }
}