package edu.netcracker.lecture2;

public class TurnCommand implements RoverCommand {
    private Turnable turnable;
    private Direction direction;

    public TurnCommand(Turnable turnable) {
        this.turnable = turnable;
    }

    public TurnCommand(Turnable turnable, Direction direction) {
        this.turnable = turnable;
        this.direction = direction;
    }

    @Override
    public void execute() {
        turnable.turnTo(direction);
    }

    @Override
    public void append(RoverCommand subcommands) {
        throw new UnsupportedOperationException();
    }

    @Override
    public RoverCommand getRoverCommand(int index) {
        throw new UnsupportedOperationException();
    }

    public String toString() {
        return "turn " + direction;
    }
}
