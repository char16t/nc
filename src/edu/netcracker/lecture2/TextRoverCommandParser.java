package edu.netcracker.lecture2;

import java.io.*;
import java.util.LinkedList;
import java.util.List;

public class TextRoverCommandParser {
    private Rover rover;
    private String file;
    private int cursor;
    private List<RoverCommand> roverCommands = new LinkedList<RoverCommand>();

    public TextRoverCommandParser(Rover rover) throws IOException {
        this.rover = rover;
        this.cursor = 0;
        this.file = null;
    }

    public void setFile(String file) {
        this.file = file;
        try {
            initRoverCommands();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public RoverCommand readNextCommand() {
        if (cursor < roverCommands.size()) {
            return roverCommands.get(this.cursor++);
        }
        return null;
    }

    public List<RoverCommand> getRoverCommands() {
        return roverCommands;
    }

    public void addRoverCommands(RoverCommand ... cmds) throws IOException {
        for (RoverCommand command : cmds) {
            this.roverCommands.add(command);
            if (command instanceof ImportCommand) command.execute();
        }

    }

    public void addRoverCommands(List<RoverCommand> newRoverCommands) {
        for (RoverCommand command : newRoverCommands) {
            this.roverCommands.add(command);
        }
    }

    private void initRoverCommands() throws IOException {
        if (this.file == null) {
            return;
        }

        FileInputStream fstream = new FileInputStream(this.file);
        BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
        String strLine;

        while ((strLine = br.readLine()) != null) {
            String[] parts = strLine.split(" ");
            if(parts[0] == "move") {
                this.roverCommands.add(new MoveCommand(this.rover, Integer.parseInt(parts[1]), Integer.parseInt(parts[2])));
            }
            else if (parts[0] == "turn") {
                this.roverCommands.add(new TurnCommand(this.rover, Direction.valueOf(parts[1])));
            }
        }

        fstream.close();
    }
}
