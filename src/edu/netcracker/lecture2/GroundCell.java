package edu.netcracker.lecture2;

public class GroundCell {
    private CellState state;
    private int x;
    private int y;

    public GroundCell(int x, int y) {
        this.state = CellState.FREE;
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public CellState getState() {
        return state;
    }

    public void setState(CellState state) {
        this.state = state;
    }
}