package edu.netcracker.lecture2;

public class GroundVisor {
    private Ground ground;

    public GroundVisor() {
        this.ground = null;
    }

    public GroundVisor(Ground ground) {
        this.ground = ground;
    }

    public boolean hasObstacles(int length, int width) throws GroundVisorException {
        if (length > ground.getLength() || width > ground.getWidth()) {
            throw new GroundVisorException();
        }

        return ground.getLandscape()[length][width].getState() == CellState.OCCUPIED;
    }

    public void setGround(Ground ground) {
        this.ground = ground;
    }

    public Ground getGround() {
        return ground;
    }
}
